fsiSixDoFRigidBodyMotion
========================

## General

The fsiSixDoFRigidBodyMotion library is applying the IQN-ILS coupling algorithm 
for the acceleration and torque to have an accelerated fluid--motion coupling.

The IQN-ILS coupling algorithm is implemented as described in "Degroote J. (2013). 
Partitioned Simulation of Fluid-Structure Interaction. Archives of Computational 
Methods in Engineering, 20, 185–238. [doi:10.1007/s11831-013-9085-5](https://doi.org/10.1007/s11831-013-9085-5)".

## History

- Developed initially for **OpenFOAM-v1806**
- Upgraded to **OpenFOAM-v1812** on 15-JAN-2019

## References and citing

The model has been developed within: 
- the PhD thesis of Brecht Devolder at the Department of Civil Engineering at 
Ghent University and KU Leuven, funded by the Research Foundation – 
Flanders (FWO), Belgium (Ph.D. fellowship 1133817N). The PhD thesis is available 
for download at: <https://biblio.ugent.be/publication/8564551>. 
- the postdoctoral research project of Brecht Devolder at the department 
of civil engineering at KU Leuven, campus Bruges. The project is a collaboration 
between KU Leuven and DEME and is funded by the agency Flanders Innovation 
& Entrepreneurship (VLAIO) and DEME.

If you want to reference the model in your publications, you can use the 
following references in which the implementation and validation details 
are published:  
- Devolder, B. (2018). Hydrodynamic Modelling of Wave Energy Converter Arrays. PhD thesis. Ghent University, Faculty of Engineering and Architecture, Ghent, Belgium. KU Leuven, Faculty of Engineering Technology, Bruges, Belgium.
- Devolder, B., Troch, P., & Rauwoens, P. (2019). Accelerated numerical simulations of a heaving floating body by coupling a motion solver with a two-phase fluid solver. Computers and Mathematics with Applications, 77(6), 1605–1625. [doi:10.1016/j.camwa.2018.08.064](https://doi.org/10.1016/j.camwa.2018.08.064).
- Devolder, B., Stempinski, F., Mol, A., Rauwoens, P. (2019). Validation of a roll decay test of an offshore installation vessel using OpenFOAM. International Conference on Computational Methods in Marine Engineering (pp. 658–669). Presented at the International Conference on Computational Methods in Marine Engineering (MARINE 2019).

## Installation for OpenFOAM ESI OpenCFD releases

- Open a linux terminal and download the package using git:

      git clone https://gitlab.com/brecht.devolder/fsiSixDoFRigidBodyMotion.git
      cd fsiSixDoFRigidBodyMotion

- Source the OpenFOAM environment: 

      source $HOME/OpenFOAM/OpenFOAM-vYYMM/etc/bashrc        
      
- Compile the source code to build a shared library:

      wmake libso

## How to use

Add the following lines in constant/dynamicMeshDict

      motionSolverLibs    ("libfsiSixDoFRigidBodyMotion.so");
      
      sixDoFRigidBodyMotionCoeffs
      {
		...
      	accelerationRelaxation     1.0;
      	accelerationDamping        1.0;
      	fsiRelaxationAcceleration  0.2;
      	fsiResidualAcceleration    1e-2;
      	fsiRelaxationRotation      0.2;
      	fsiResidualRotation        1e-2;
      	...
      }

## Contact

Brecht Devolder (PhD, MSc)  
Department of Civil Engineering - Construction TC  
KU Leuven - Campus Bruges  
Spoorwegstraat 12, B-8200 Brugge, Belgium  
<brecht.devolder@kuleuven.be>  
