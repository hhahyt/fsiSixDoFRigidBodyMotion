/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2011-2017 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "sixDoFRigidBodyMotion.H"
#include "sixDoFSolver.H"
#include "septernion.H"

// * * * * * * * * * * * * * Private Member Functions  * * * * * * * * * * * //

void Foam::sixDoFRigidBodyMotion::applyRestraints()
{
    if (restraints_.empty())
    {
        return;
    }

    if (Pstream::master())
    {
        forAll(restraints_, rI)
        {
            if (report_)
            {
                Info<< "Restraint " << restraints_[rI].name() << ": ";
            }

            // Restraint position
            point rP = Zero;

            // Restraint force
            vector rF = Zero;

            // Restraint moment
            vector rM = Zero;

            // Accumulate the restraints
            restraints_[rI].restrain(*this, rP, rF, rM);

            // Update the acceleration
            a() += rF/mass_;

            // Moments are returned in global axes, transforming to
            // body local to add to torque.
            tau() += Q().T() & (rM + ((rP - centreOfRotation()) ^ rF));
        }
    }
}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::sixDoFRigidBodyMotion::sixDoFRigidBodyMotion()
:
    motionState_(),
    motionState0_(),
    restraints_(),
    constraints_(),
    tConstraints_(tensor::I),
    rConstraints_(tensor::I),
    initialCentreOfMass_(Zero),
    initialCentreOfRotation_(Zero),
    initialQ_(I),
    mass_(VSMALL),
    momentOfInertia_(diagTensor::one*VSMALL),
    aRelax_(1.0),
    xRelax_(1.0),
    xResMax_(SMALL),
    yRelax_(1.0),
    yResMax_(SMALL),
    aDamp_(1.0),
    report_(false),
    solver_(nullptr)
{}


Foam::sixDoFRigidBodyMotion::sixDoFRigidBodyMotion
(
    const dictionary& dict,
    const dictionary& stateDict
)
:
    motionState_(stateDict),
    motionState0_(),
    restraints_(),
    constraints_(),
    tConstraints_(tensor::I),
    rConstraints_(tensor::I),
    initialCentreOfMass_
    (
        dict.lookupOrDefault
        (
            "initialCentreOfMass",
            vector(dict.lookup("centreOfMass"))
        )
    ),
    initialCentreOfRotation_(initialCentreOfMass_),
    initialQ_
    (
        dict.lookupOrDefault
        (
            "initialOrientation",
            dict.lookupOrDefault("orientation", tensor::I)
        )
    ),
    mass_(readScalar(dict.lookup("mass"))),
    momentOfInertia_(dict.lookup("momentOfInertia")),
    aRelax_(dict.lookupOrDefault<scalar>("accelerationRelaxation", 1.0)),
    xRelax_(dict.lookupOrDefault<scalar>("fsiRelaxationAcceleration", 1.0)),
    xResMax_(dict.lookupOrDefault<scalar>("fsiResidualAcceleration", SMALL)),
    yRelax_(dict.lookupOrDefault<scalar>("fsiRelaxationRotation", 1.0)),
    yResMax_(dict.lookupOrDefault<scalar>("fsiResidualRotation", SMALL)),
    aDamp_(dict.lookupOrDefault<scalar>("accelerationDamping", 1.0)),
    report_(dict.lookupOrDefault("report", false)),
    solver_(sixDoFSolver::New(dict.subDict("solver"), *this))
{
    addRestraints(dict);

    // Set constraints and initial centre of rotation
    // if different to the centre of mass
    addConstraints(dict);

    // If the centres of mass and rotation are different ...
    vector R(initialCentreOfMass_ - initialCentreOfRotation_);
    if (magSqr(R) > VSMALL)
    {
        // ... correct the moment of inertia tensor using parallel axes theorem
        momentOfInertia_ += mass_*diag(I*magSqr(R) - sqr(R));

        // ... and if the centre of rotation is not specified for motion state
        // update it
        if (!stateDict.found("centreOfRotation"))
        {
            motionState_.centreOfRotation() = initialCentreOfRotation_;
        }
    }

    // Save the old-time motion state
    motionState0_ = motionState_;
}


Foam::sixDoFRigidBodyMotion::sixDoFRigidBodyMotion
(
    const sixDoFRigidBodyMotion& sDoFRBM
)
:
    motionState_(sDoFRBM.motionState_),
    motionState0_(sDoFRBM.motionState0_),
    restraints_(sDoFRBM.restraints_),
    constraints_(sDoFRBM.constraints_),
    tConstraints_(sDoFRBM.tConstraints_),
    rConstraints_(sDoFRBM.rConstraints_),
    initialCentreOfMass_(sDoFRBM.initialCentreOfMass_),
    initialCentreOfRotation_(sDoFRBM.initialCentreOfRotation_),
    initialQ_(sDoFRBM.initialQ_),
    mass_(sDoFRBM.mass_),
    momentOfInertia_(sDoFRBM.momentOfInertia_),
    aRelax_(sDoFRBM.aRelax_),
    xRelax_(sDoFRBM.xRelax_),
    xResMax_(sDoFRBM.xResMax_),
    yRelax_(sDoFRBM.yRelax_),
    yResMax_(sDoFRBM.yResMax_),
    aDamp_(sDoFRBM.aDamp_),
    report_(sDoFRBM.report_),
    solver_(sDoFRBM.solver_.clone())
{}


// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

Foam::sixDoFRigidBodyMotion::~sixDoFRigidBodyMotion()
{} // Define here (incomplete type in header)


// * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * * //

void Foam::sixDoFRigidBodyMotion::addRestraints
(
    const dictionary& dict
)
{
    if (dict.found("restraints"))
    {
        const dictionary& restraintDict = dict.subDict("restraints");

        label i = 0;

        restraints_.setSize(restraintDict.size());

        forAllConstIter(IDLList<entry>, restraintDict, iter)
        {
            if (iter().isDict())
            {
                restraints_.set
                (
                    i++,
                    sixDoFRigidBodyMotionRestraint::New
                    (
                        iter().keyword(),
                        iter().dict()
                    )
                );
            }
        }

        restraints_.setSize(i);
    }
}


void Foam::sixDoFRigidBodyMotion::addConstraints
(
    const dictionary& dict
)
{
    if (dict.found("constraints"))
    {
        const dictionary& constraintDict = dict.subDict("constraints");

        label i = 0;

        constraints_.setSize(constraintDict.size());

        pointConstraint pct;
        pointConstraint pcr;

        forAllConstIter(IDLList<entry>, constraintDict, iter)
        {
            if (iter().isDict())
            {
                constraints_.set
                (
                    i,
                    sixDoFRigidBodyMotionConstraint::New
                    (
                        iter().keyword(),
                        iter().dict(),
                        *this
                    )
                );

                constraints_[i].setCentreOfRotation(initialCentreOfRotation_);
                constraints_[i].constrainTranslation(pct);
                constraints_[i].constrainRotation(pcr);

                i++;
            }
        }

        constraints_.setSize(i);

        tConstraints_ = pct.constraintTransformation();
        rConstraints_ = pcr.constraintTransformation();

        Info<< "Translational constraint tensor " << tConstraints_ << nl
            << "Rotational constraint tensor " << rConstraints_ << endl;
    }
}


void Foam::sixDoFRigidBodyMotion::updateAcceleration
(
    bool firstIter,
    const vector& fGlobal,
    const vector& tauGlobal
)
{
    static bool first = true;

    // Save the previous iteration accelerations for relaxation
    vector aPrevIter = a();
    vector tauPrevIter = tau();

    // Calculate new accelerations
    a() = fGlobal/mass_;
    tau() = (Q().T() & tauGlobal);
    applyRestraints();

    // Relax accelerations on all but first iteration
    if (!firstIter)
    {
        a() = aRelax_*a() + (1 - aRelax_)*aPrevIter;
        tau() = aRelax_*tau() + (1 - aRelax_)*tauPrevIter;
    }
    else
    {
        first = false;
    }
}


Foam::Tuple2<Foam::vector, Foam::vector> Foam::sixDoFRigidBodyMotion::fsiCoupling
(
    bool firstIter,
    Tuple2<vector, vector>& xy
)
{
	// Implement of Aitken relaxation and IQN-ILS according to Degroote (2013):
	// Partitioned Simulation of Fluid-Structure Interaction, page 220 and 221
	
	vector x = xy.first();
    vector y = xy.second();
    
    //WorkPackage: make a 6D vector for the coupling algorithm instead of two 3D vectors
    /*
    Matrix <scalarField,scalar> sixDoFvector (1,6);
    sixDoFvector(0,0) = x.x();
    sixDoFvector(0,1) = x.y();
    sixDoFvector(0,2) = x.z();
    sixDoFvector(0,3) = y.x();
    sixDoFvector(0,4) = y.y();
    sixDoFvector(0,5) = y.z();
    
    Info<< "sixDoFvector: " << sixDoFvector << endl;
    */
    
    // Calculate residuals
    vector xResPrevIter = xRes();
    vector yResPrevIter = yRes();
    xRes() = x - xPrevIter();  
    yRes() = y - yPrevIter();  
    
    // Aitken relaxation coefficient (scalar)
    scalar xCoeffAitken = ( ((x - xTilde()) & (xRes() - xResPrevIter)) / ((xRes() - xResPrevIter) & (xRes() - xResPrevIter)) - 1.0 ) ;
    scalar yCoeffAitken = ( ((y - yTilde()) & (yRes() - yResPrevIter)) / ((yRes() - yResPrevIter) & (yRes() - yResPrevIter)) - 1.0 ) ;
    
    // IQN-ILS relaxation coefficient (tensor)
    tensor xCoeffIQNILS = ( ((x - xTilde()) * (xRes() - xResPrevIter)) / ((xRes() - xResPrevIter) & (xRes() - xResPrevIter)) - tensor::I ) ;
    tensor yCoeffIQNILS = ( ((y - yTilde()) * (yRes() - yResPrevIter)) / ((yRes() - yResPrevIter) & (yRes() - yResPrevIter)) - tensor::I ) ;

    // Unrelaxed value for next sub iteration (Aitken and IQN-ILS)
    xTilde() = x;
    yTilde() = y;

    // Relaxation parameter depends on number of iteration within a time step
    if (firstIter) // first sub iteration
    {	
		// During first sub iteration is no relaxation required	
		// Set xRes() equal to zero for next sub iteration
		xRes() = vector::zero;
		yRes() = vector::zero;
		
	}
    else if (xResPrevIter == vector::zero) // second sub iteration
    {
        // During second iteration, relax x
        x = xRelax_*x + (1.0 - xRelax_)*xPrevIter();
        y = yRelax_*y + (1.0 - yRelax_)*yPrevIter();
    }
    else
    {
		
		Info<< "FSI coupling" << nl
		<< "    fsiAccelerationRes:     " << xRes()/mag(x) << nl
		<< "    fsiAccelerationResNorm: " << mag(xRes())/mag(x) << nl
		<< "    fsiRotationRes:         " << yRes()/mag(y) << nl
		<< "    fsiRotationResNorm:     " << mag(yRes())/mag(y)
		<< endl;
		
	    if (mag(xRes())/mag(x) < xResMax_ and mag(yRes())/mag(y) < yResMax_) //normalised residuals!
	    {
			x = xPrevIter();
			y = yPrevIter();
		}
		else
		{   
            // Relax x and y (IQN-ILS)
            x = xPrevIter() - (xCoeffIQNILS & xRes()) ;
            y = yPrevIter() - (yCoeffIQNILS & yRes()) ;
	    }
    }
    
    // Save the 2nd previous iteration accelerations for relaxation
    xPrevPrevIter() = xPrevIter();
    yPrevPrevIter() = yPrevIter();

    // Save the previous iteration accelerations for relaxation
    xPrevIter() = x;
    yPrevIter() = y;
    
    xy.first() = x;
    xy.second() = y;
       
    return xy;
    
}


void Foam::sixDoFRigidBodyMotion::update
(
    bool firstIter,
    const vector& fGlobal,
    const vector& tauGlobal,
    scalar deltaT,
    scalar deltaT0
)
{
    if (Pstream::master())
    {        
        // Print a, v and x for post processing
	    if (firstIter)
	    {
            vector angles = quaternion(orientation()).eulerAngles(quaternion::rotationSequence::XYZ);
            
            Info<< "Plot a: " << a() << endl;
            Info<< "Plot v: " << v() << endl;
            Info<< "Plot x: " << centreOfRotation() << endl;
            Info<< "Plot Euler angles: " << angles << endl;
	    }
	    
        solver_->solve(firstIter, fGlobal, tauGlobal, deltaT, deltaT0);

        if (report_)
        {
            status();
        }

    }

    Pstream::scatter(motionState_);
}


void Foam::sixDoFRigidBodyMotion::status() const
{
    Info<< "6-DoF rigid body motion     " << nl
        << "    Centre of rotation:     " << centreOfRotation() << nl
        << "    Centre of mass:         " << centreOfMass() << nl
        << "    Orientation:            " << orientation() << nl
        << "    Linear velocity:        " << v() << nl
        << "    Angular velocity:       " << omega() << nl
        << "    Angular momentum:       " << pi() << nl
        << "    Linear acceleration:    " << a() << nl
        << "    Torque:                 " << tau() << nl
        //<< "FSI coupling" << nl
        //<< "    fsiAccelerationRes:     " << xRes() << nl
        //<< "    fsiAccelerationResNorm: " << mag(xRes()) << nl
        //<< "    fsiRotationRes:         " << yRes() << nl
        //<< "    fsiRotationResNorm:     " << mag(yRes())
        << endl;
}


Foam::tmp<Foam::pointField> Foam::sixDoFRigidBodyMotion::transform
(
    const pointField& initialPoints
) const
{
    return
    (
        centreOfRotation()
      + (Q() & initialQ_.T() & (initialPoints - initialCentreOfRotation_))
    );
}


Foam::tmp<Foam::pointField> Foam::sixDoFRigidBodyMotion::transform
(
    const pointField& initialPoints,
    const scalarField& scale
) const
{
    // Calculate the transformation septerion from the initial state
    septernion s
    (
        centreOfRotation() - initialCentreOfRotation(),
        quaternion(Q().T() & initialQ())
    );

    tmp<pointField> tpoints(new pointField(initialPoints));
    pointField& points = tpoints.ref();

    forAll(points, pointi)
    {
        // Move non-stationary points
        if (scale[pointi] > SMALL)
        {
            // Use solid-body motion where scale = 1
            if (scale[pointi] > 1 - SMALL)
            {
                points[pointi] = transform(initialPoints[pointi]);
            }
            // Slerp septernion interpolation
            else
            {
                septernion ss(slerp(septernion::I, s, scale[pointi]));

                points[pointi] =
                    initialCentreOfRotation()
                  + ss.invTransformPoint
                    (
                        initialPoints[pointi]
                      - initialCentreOfRotation()
                    );
            }
        }
    }

    return tpoints;
}


// ************************************************************************* //
