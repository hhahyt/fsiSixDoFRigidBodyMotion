/*---------------------------------------------------------------------------*\
  =========                 |
  \\      /  F ield         | OpenFOAM: The Open Source CFD Toolbox
   \\    /   O peration     |
    \\  /    A nd           | Copyright (C) 2015 OpenFOAM Foundation
     \\/     M anipulation  |
-------------------------------------------------------------------------------
License
    This file is part of OpenFOAM.

    OpenFOAM is free software: you can redistribute it and/or modify it
    under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    OpenFOAM is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
    for more details.

    You should have received a copy of the GNU General Public License
    along with OpenFOAM.  If not, see <http://www.gnu.org/licenses/>.

\*---------------------------------------------------------------------------*/

#include "rotateBody.H"
#include "addToRunTimeSelectionTable.H"

// * * * * * * * * * * * * * * Static Data Members * * * * * * * * * * * * * //

namespace Foam
{
namespace sixDoFSolvers
{
    defineTypeNameAndDebug(rotateBody, 0);
    addToRunTimeSelectionTable(sixDoFSolver, rotateBody, dictionary);
}
}


// * * * * * * * * * * * * * * * * Constructors  * * * * * * * * * * * * * * //

Foam::sixDoFSolvers::rotateBody::rotateBody
(
    const dictionary& dict,
    sixDoFRigidBodyMotion& body
)
:
    sixDoFSolver(dict, body),
    rotX_(dict.lookupOrDefault<scalar>("rotX", 0.0)),
    rotY_(dict.lookupOrDefault<scalar>("rotY", 0.0)),
    rotZ_(dict.lookupOrDefault<scalar>("rotZ", 0.0))
{}


// * * * * * * * * * * * * * * * * Destructor  * * * * * * * * * * * * * * * //

Foam::sixDoFSolvers::rotateBody::~rotateBody()
{}


// * * * * * * * * * * * * * * * Member Functions  * * * * * * * * * * * * * //

void Foam::sixDoFSolvers::rotateBody::solve
(
    bool firstIter,
    const vector& fGlobal,
    const vector& tauGlobal,
    scalar deltaT,
    scalar deltaT0
)
{
    tensor xT
    (
        1, 0, 0,
        0, Foam::cos(0.5*rotX_), -Foam::sin(0.5*rotX_),
        0, Foam::sin(0.5*rotX_), Foam::cos(0.5*rotX_)
    );

    tensor yT
    (
        Foam::cos(0.5*rotY_), 0, Foam::sin(0.5*rotY_),
        0, 1, 0,
        -Foam::sin(0.5*rotY_), 0, Foam::cos(0.5*rotY_)
    );

    tensor zT
    (
        Foam::cos(rotZ_), -Foam::sin(rotZ_), 0,
        Foam::sin(rotZ_), Foam::cos(rotZ_), 0,
        0, 0, 1
    );
    
    Q() = xT & yT & zT & yT & xT;
    
}

// ************************************************************************* //
